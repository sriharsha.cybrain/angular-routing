import { Component } from '@angular/core';

@Component({
  selector: 'app-databinding',
  template:`
  
  <!-- Interpolation -->
  <h2>{{firstname}}</h2>
  <h2>{{lastname.length}}</h2>
  <h2>{{firstname+lastname}}</h2>
  
  <!-- Event Binding -->
  <button (click)="ONCLICK()">Greet</button>
  <h3>{{eventbinding}}</h3>
  <button (click)="onClick($event.clientX, $event.clientY)">Click me!</button>

  <!-- Property Binding -->
  <h1>Person Details</h1>
    <p>Name: {{ person.name }}</p>
    <p>Age: {{ person.age }}</p>

  <!-- Class Binding -->
  <h3 [class]="killjoy">Harsha</h3>
  <h2 class="sova">me only</h2>
  
  `,
  styles: [`
  /* class binding style */
  .reyna
  {
  text-align:center;
  } 
  .sova
  {
    color:red;
  }`]
})
export class DatabindingComponent {
  public firstname="Sriharsha"
  public lastname="CP"
  
  public killjoy="reyna"

  eventbinding!: string;
  ONCLICK(){

    this.eventbinding = 'hey u clicked me wow'
  
   }
   onClick(x: number, y: number) {
    this.eventbinding =`Button clicked at (${x}, ${y})!`
   }

  person = {
    name: 'Priya',
    age: 22
  };

  

}
