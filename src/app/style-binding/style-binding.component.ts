import { Component } from '@angular/core';

@Component({
  selector: 'app-style-binding',
  template:`

  <!-- ngStyle -->
  <div [ngStyle]="{ color: isActive ? 'green' : 'red', 'font-size.px': 25 }">
    Have you watched F.R.I.E.N.D.S? Friends is an American television sitcom created by David Crane and Marta Kauffman, which aired on NBC from September 22, 1994, to May 6, 2004, lasting ten seasons.[1] With an ensemble cast starring Jennifer Aniston, Courteney Cox, Lisa Kudrow, Matt LeBlanc, Matthew Perry and David Schwimmer, the show revolves around six friends in their 20s and 30s who live in Manhattan, New York City. The series was produced by Bright/Kauffman/Crane Productions, in association with Warner Bros. Television. The original executive producers were Kevin S. Bright, Kauffman, and Crane.
  </div>

  <!-- ngclass -->
  <h2> ngClass </h2>
  <button [ngClass]="{ 'active': iamactivebro }" (click) = "clickme()">Click me!</button>
  <p>{{work}}</p>

  `,
  styleUrls: ['./style-binding.component.scss']
})
export class StyleBindingComponent {
  iamactivebro=true;
  isActive=true;

  work!:string
  clickme(){
this.work = 'how r u doing man'
  }
}
